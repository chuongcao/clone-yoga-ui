import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../providers/data.provider.dart';

class Test extends ConsumerWidget {
   const Test({Key? key}) : super(key: key);

   @override
   Widget build(BuildContext context, WidgetRef ref) {
      final data = ref.watch(CoursesNotifier.provider);
      
      return Text(data.length.toString());
   }
}