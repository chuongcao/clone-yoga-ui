
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/course.model.dart';
import '../providers/data.provider.dart';
import 'test.dart';

final fetchJsonProvider = FutureProvider((ref) async {
   final String response = await rootBundle.loadString('assets/json/contentData.json');
   final dataJson = await jsonDecode(response);

   final formattedData = dataJson is Map
      ? dataJson["items"].map<Course>((value) => Course.fromJson(value)).toList()
      : [];

   return formattedData;
});

class Dashboard extends ConsumerWidget {
   const Dashboard({Key? key}) : super(key: key);

   @override
   Widget build(BuildContext context, WidgetRef ref) {
      final data = ref.watch(fetchJsonProvider);
         
      return Scaffold(
         body: Center(
            child: data.when(
               data: (data) {                  
                  final course = ref.read(CoursesNotifier.provider.notifier);
                  course.initData(data);

                  return const Test();
               },
               loading: () {
                  return const CircularProgressIndicator.adaptive();
               },
               error: (error, stack) {
                  print(error);
                  return const Text("Error");
               }
            ),
         ),
      );
   }
}