import "package:flutter_riverpod/flutter_riverpod.dart";

import '../models/course.model.dart';

class CoursesNotifier extends StateNotifier<List<Course>> {
   static final provider =
      StateNotifierProvider<CoursesNotifier, List<Course>>((ref) {
         return CoursesNotifier();
      });

   CoursesNotifier(): super([]);

   void initData(List<Course> data) {
      state = data;
   }
}