// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'course.model.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$CourseCWProxy {
  Course catchPhrase(String catchPhrase);

  Course categoryId(String categoryId);

  Course contentId(String contentId);

  Course contentType(String contentType);

  Course displayName(String displayName);

  Course leadTitle(String leadTitle);

  Course priceJpy(int priceJpy);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `Course(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// Course(...).copyWith(id: 12, name: "My name")
  /// ````
  Course call({
    String? catchPhrase,
    String? categoryId,
    String? contentId,
    String? contentType,
    String? displayName,
    String? leadTitle,
    int? priceJpy,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfCourse.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfCourse.copyWith.fieldName(...)`
class _$CourseCWProxyImpl implements _$CourseCWProxy {
  final Course _value;

  const _$CourseCWProxyImpl(this._value);

  @override
  Course catchPhrase(String catchPhrase) => this(catchPhrase: catchPhrase);

  @override
  Course categoryId(String categoryId) => this(categoryId: categoryId);

  @override
  Course contentId(String contentId) => this(contentId: contentId);

  @override
  Course contentType(String contentType) => this(contentType: contentType);

  @override
  Course displayName(String displayName) => this(displayName: displayName);

  @override
  Course leadTitle(String leadTitle) => this(leadTitle: leadTitle);

  @override
  Course priceJpy(int priceJpy) => this(priceJpy: priceJpy);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `Course(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// Course(...).copyWith(id: 12, name: "My name")
  /// ````
  Course call({
    Object? catchPhrase = const $CopyWithPlaceholder(),
    Object? categoryId = const $CopyWithPlaceholder(),
    Object? contentId = const $CopyWithPlaceholder(),
    Object? contentType = const $CopyWithPlaceholder(),
    Object? displayName = const $CopyWithPlaceholder(),
    Object? leadTitle = const $CopyWithPlaceholder(),
    Object? priceJpy = const $CopyWithPlaceholder(),
  }) {
    return Course(
      catchPhrase:
          catchPhrase == const $CopyWithPlaceholder() || catchPhrase == null
              ? _value.catchPhrase
              // ignore: cast_nullable_to_non_nullable
              : catchPhrase as String,
      categoryId:
          categoryId == const $CopyWithPlaceholder() || categoryId == null
              ? _value.categoryId
              // ignore: cast_nullable_to_non_nullable
              : categoryId as String,
      contentId: contentId == const $CopyWithPlaceholder() || contentId == null
          ? _value.contentId
          // ignore: cast_nullable_to_non_nullable
          : contentId as String,
      contentType:
          contentType == const $CopyWithPlaceholder() || contentType == null
              ? _value.contentType
              // ignore: cast_nullable_to_non_nullable
              : contentType as String,
      displayName:
          displayName == const $CopyWithPlaceholder() || displayName == null
              ? _value.displayName
              // ignore: cast_nullable_to_non_nullable
              : displayName as String,
      leadTitle: leadTitle == const $CopyWithPlaceholder() || leadTitle == null
          ? _value.leadTitle
          // ignore: cast_nullable_to_non_nullable
          : leadTitle as String,
      priceJpy: priceJpy == const $CopyWithPlaceholder() || priceJpy == null
          ? _value.priceJpy
          // ignore: cast_nullable_to_non_nullable
          : priceJpy as int,
    );
  }
}

extension $CourseCopyWith on Course {
  /// Returns a callable class that can be used as follows: `instanceOfclass Course.name.copyWith(...)` or like so:`instanceOfclass Course.name.copyWith.fieldName(...)`.
  _$CourseCWProxy get copyWith => _$CourseCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Course _$CourseFromJson(Map<String, dynamic> json) => Course(
      contentId: json['contentID'] as String,
      categoryId: json['categoryID'] as String,
      contentType: json['contentType'] as String,
      displayName: json['displayName'] as String,
      catchPhrase: json['catchPhrase'] as String,
      leadTitle: json['leadTitle'] as String,
      priceJpy: json['priceJPY'] as int,
    );

Map<String, dynamic> _$CourseToJson(Course instance) => <String, dynamic>{
      'contentID': instance.contentId,
      'categoryID': instance.categoryId,
      'contentType': instance.contentType,
      'displayName': instance.displayName,
      'catchPhrase': instance.catchPhrase,
      'leadTitle': instance.leadTitle,
      'priceJPY': instance.priceJpy,
    };
