import 'package:copy_with_extension/copy_with_extension.dart';
import "package:json_annotation/json_annotation.dart";

part "course.model.g.dart";

@CopyWith()
@JsonSerializable()
class Course {
   @JsonKey(name: "contentID")
   String contentId;
   
   @JsonKey(name: "categoryID")
   String categoryId;

   String contentType;
   
   String displayName;

   String catchPhrase;

   String leadTitle;

   @JsonKey(name: "priceJPY")
   int priceJpy;

   Course({
      required this.contentId,
      required this.categoryId,
      required this.contentType,
      required this.displayName,
      required this.catchPhrase,
      required this.leadTitle,
      required this.priceJpy,
   });

   factory Course.fromJson(Map<String, dynamic> json) => _$CourseFromJson(json);

   Map<String, dynamic> toJson() => _$CourseToJson(this);
}