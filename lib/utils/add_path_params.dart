String addPathParam({
   required String key,
   String? value
}) {
   return value != null
      ? "$key=$value&"
      : "";
}